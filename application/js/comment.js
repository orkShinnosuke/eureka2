(function() {
    var ajs = angular.module('myApp.controllers', []);
    ajs.controller('AppController', function($scope, $rootScope, $http) {
        $rootScope.appUrl = "http://orkmacplay.local/eureka2/index.php";
    });
 
    ajs.controller('CommentController', function($scope, $rootScope, $http) {

        // 登録(comment_confirm)
        $scope.addComment = function() {
            $http.post($rootScope.appUrl + '/thread/add', {
                'commentContent': $scope.commentContent,
                'thrId': $scope.thrId,
            }).success(function(){
                alert('登録しました。');
            }).error(function(data, status) {
                // $scope.errors = data;
                alert('失敗しました。');
            });
        };
    });

}());