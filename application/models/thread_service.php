<?php  
class Thread_service extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    # 上位10件のスレのスレ取得
    public function getThreadsTop10()
    {
        # threadsテーブルからデータ取得
        $this->load->model('thread_dao');

        $limit = 10;
        $fields = 'id, user_id, title, content, created';
        $threadsTop10 = $this->thread_dao->getThreadOrderByModified($limit, $fields);

        return $threadsTop10;
    }

    # 上位10件のスレへのコメント
    public function getComments($threads)
    {
        $comments = array();
        $fields = 'id, thread_id, user_id, content, picture, fb_picture, created';
        foreach ($threads as $thread) {
            $threadId = $thread->id;
            $comments[$threadId] = $this->thread_dao->getThreadComments($threadId, $fields);
        }

        return $comments;
    }

    # 上位100件のスレタイトル
    public function getThreadsTop100()
    {
        # threadsテーブルからデータ取得
        $this->load->model('thread_dao');
        # 上位100件のスレタイトル
        $limit = 100;
        $fields = 'id, title';
        $threadsTop100 = $this->thread_dao->getThreadOrderByModified($limit, $fields);

        return $threadsTop100;
    }

}
