<?php  
class User_dao extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getUserByFacebookId($fbId, $fields)
    {
        $this->load->database();
        $this->db->where(array('facebook_id' => $fbId));
        $this->db->select($fields);
        $query = $this->db->get('users');
        $results = $query->result();

        return $results;
    }

    public function insertUser($me)
    {
        # facebookID
        $this->db->set('facebook_id', $me['id']);
        # 名前
        $this->db->set('lastname', $me['last_name']);
        $this->db->set('firstname', $me['first_name']);
        # 性別
        $this->db->set('gender', $me['gender']);
        # プロフィール画像
        $this->db->set('profile_img', 'https://graph.facebook.com/'.$me['id'].'/picture');
        # 作成日時 & 更新日時
        $this->db->set('created', date('Y-m-d H:i:s'));
        $this->db->set('modified', date('Y-m-d H:i:s'));

        $this->db->insert('users');
    }
}
