<?php  
class User_service extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getUserByFacebookId($fbId, $fields)
    {
        $this->load->database();
        $this->db->where(array('facebook_id' => $fbId));
        $this->db->select($fields);
        $query = $this->db->get('users');
        $results = $query->result();

        return $results;
    }
}
