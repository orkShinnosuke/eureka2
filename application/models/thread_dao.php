<?php  
class Thread_dao extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getById($thrId)
    {
        $this->load->database();
        $this->db->where(array('deleted' => 0));
        $this->db->where(array('id' => $thrId));
        $fields = 'id, user_id, title, content, created';
        $this->db->select($fields);
        $query = $this->db->get('threads');
        
        return $query->result();
    }

    public function getUserByFacebookId($fbId, $fields)
    {
        $this->load->database();
        $this->db->where(array('facebook_id' => $fbId));
        $this->db->select($fields);
        $query = $this->db->get('users');

        return $query->result();
    }

    public function getThreadOrderByModified($limit = null, $fields = null)
    {
        $this->load->database();
        $this->db->where(array('deleted' => 0));
        $this->db->order_by('modified desc');
        $this->db->limit($limit);
        $this->db->select($fields);
        $query = $this->db->get('threads');

        return $query->result();
    }

    public function getThreadComments($threadId, $fields = null)
    {
        $this->load->database();
        $this->db->where(array('deleted' => 0, 'thread_id' => $threadId));
        $this->db->select($fields);
        $query = $this->db->get('comments');

        return $query->result();
    }

    public function insertThread($postData, $userId)
    {
        $title = $postData['title'];
        $content = $postData['content'];
        $this->load->database();
        $this->db->set('user_id', $userId);
        $this->db->set('title', $title);
        $this->db->set('content', $content);
        $this->db->set('created', date('Y-m-d H:i:s'));
        $this->db->set('modified', date('Y-m-d H:i:s'));
        $this->db->insert('threads');
    }
}
