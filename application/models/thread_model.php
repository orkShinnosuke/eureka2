<?php  
class Thread_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getUserByFacebookId($fbId, $fields)
    {
        $this->load->database();
        $this->db->where(array('facebook_id' => $fbId));
        $this->db->select($fields);
        $query = $this->db->get('users');
        $results = $query->result();

        return $results;
    }

    public function getThreadOrderByModified($limit = null, $fields = null)
    {
        // $options = array(
        //     'conditions' => array(
        //         'Thread.deleted' => 0,
        //     ),
        //     'order' => array(
        //         'Thread.modified' => 'DESC',
        //     ),
        // );
        // if ($limit != null) {
        //     $options['limit'] = $limit;
        // }
        // if ($fields != null) {
        //     $options['fields'] = $fields;
        // }
        // $data = $this->find('all', $options);
        // return $data;

        $this->load->database();
        $this->db->where(array('deleted' => 0));
        $this->db->order_by('modified desc');
        $this->db->limit($limit);
        $this->db->select($fields);
        $query = $this->db->get('threads');
        return $query->result();
    }

    public function getThreadComments($threadId, $fields = null)
    {
        $this->load->database();
        $this->db->where(array('deleted' => 0, 'thread_id' => $threadId));
        $this->db->select($fields);
        $query = $this->db->get('comments');
        return $query->result();
    }
}
