<?php  
class Comment_dao extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function insertComment($postData, $userId)
    {
        $thrId = $postData['thrId'];
        $commentContent = $postData['commentContent'];
        $this->load->database();
        $this->db->set('user_id', $userId);
        $this->db->set('thread_id', $thrId);
        $this->db->set('content', $commentContent);
        $this->db->set('created', date('Y-m-d H:i:s'));
        $this->db->set('modified', date('Y-m-d H:i:s'));
        $this->db->insert('comments');
    }
}
