<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>エウレカ課題</title>
    <!-- 自作CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/eureka.css" />
    <!-- Angular.js -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.min.js"></script>
    <script src="<?php echo base_url(); ?>js/app.js"></script>
    <script src="<?php echo base_url(); ?>js/comment.js"></script>
</head>
<body>
    <?php echo $this->load->view('header', $fb); ?>

    <div ng-app="myApp" class="container" style="margin-top: 70px; margin-bottom: 70px;">
        <div ng-controller="AppController">
            <div ng-controller="CommentController">

                <?php  
                echo anchor('thread/detail/'.$thrId, 'スレに移動', array('class' => 'btn btn-default'));
                ?>
                <br><br>
                <div class="bigger-comment-row">
                    <div class="commentHeader">
                        <?php 
                        # コメント番号
                        echo $comNum;
                        echo '.　';
                        # コメント日時
                        echo $comment->created;
                        echo '　ID: ';
                        # コメント者
                        echo $comment->user_id; 
                        ?>
                    </div>
                    <div class="commentContent">
                        <?php 
                        # コメント内容(「>>」のような特殊文字のアンエスケープ)
                        $commentContent = htmlspecialchars_decode($comment->content);
                        # 「>>数値」を含むコメントの時
                        if (preg_match('/>>([0-9]+)/', $commentContent, $matches)) {                
                            //var_dump($matches);
                            # 「>>数値」だけコメント内容から切り出して、リンクとして表示
                            echo anchor('comment/detail/'.$thrId.'/'.$matches[1], $matches[0]); 
                            # 「>>数値」をコメント内容から削除
                            $commentContent = str_replace($matches[0], '', $commentContent);
                            echo $commentContent;
                        } else {
                            echo $commentContent;
                        }
                        ?>    
                    </div>    
                </div>
            </div>
        </div>
    </div>
</body>
</html>