<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>エウレカ課題</title>
    <!-- 自作CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/eureka.css" />
    <!-- Angular.js -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.min.js"></script>
    <script src="<?php echo base_url(); ?>js/app.js"></script>
    <script src="<?php echo base_url(); ?>js/thread.js"></script>
</head>
<body>
    <?php echo $this->load->view('header', $fb); ?>

    <div ng-app="myApp" class="container" style="margin-top: 70px; margin-bottom: 70px;">

        <div ng-controller="AppController">

            <div ng-controller="ThreadController">

                <?php
                # 最新10件のスレの内容一覧
                $thrUserId = $thread[0]->user_id;
                $thrId = $thread[0]->id;
                $thrTitle = $thread[0]->title;
                $thrContent = $thread[0]->content;
                $thrCreated = $thread[0]->created;
                ?>
                <div class="thread-row">
                    <div class="threadTitle">
                        <?php 
                        # スレタイトル
                        echo $thrTitle; 
                        ?>
                    </div>
                    <div class="threadHeader">
                        <?php 
                        # コメント番号(スレだからコメント番号は必ず「1」)
                        echo 1;
                        echo '.　';
                        # スレ作成日時
                        echo $thrCreated;
                        echo '　ID: ';
                        # スレ作成者
                        echo $thrUserId;
                        ?>
                    </div>
                    <div class="threadContent">
                        <?php 
                        # スレ内容
                        echo $thrContent; 
                        ?>
                    </div>
                    <?php 
                    # 一番大きなコメント番号(スレ内容自体をコメントとみなすので+1)
                    // $thrComments = $comments[$thrId];
                    $biggestCommentNum = count($comments) + 1;
                    foreach ($comments as $commentKey => $comment): 
                    ?>
                        <hr>
                        <div class="commentHeader">
                            <?php 
                            # コメント番号
                            $commentNumSelf = $commentKey+2;
                            echo $commentNumSelf;
                            echo '.　';
                            # コメント日時
                            echo $comment->created;
                            echo '　ID: ';
                            # コメント者
                            echo $comment->user_id; 
                            ?>
                        </div>
                        <div class="commentContent">
                            <?php 
                            # コメント内容
                            #「>>」のような特殊文字のアンエスケープ
                            $commentContent = htmlspecialchars_decode($comment->content);
                            # 「>>数値」を含むコメントの時
                            if (preg_match('/>>([0-9]+)/', $commentContent, $matches)) {
                                # 「>>数値」
                                $linkStr = $matches[0]; 
                                # コメント番号
                                $commentNum = $matches[1];

                                # コメント番号が1の時
                                if ($commentNum == 1) {
                                    # 「>>数値」だけコメント内容から切り出して、リンクとして表示
                                    echo anchor('thread/detail/'.$thrId, $linkStr); 
                                    # 「>>数値」をコメント内容から削除
                                    $commentContent = str_replace($linkStr, '', $commentContent);
                                # コメント番号が実在する時 かつ 対象のコメント番号が自信のコメント番号より小さい時、リンクを表示
                                } elseif ((2 <= $commentNum && $commentNum <= $biggestCommentNum) && $commentNum < $commentNumSelf) {           
                                    # 「>>数値」だけコメント内容から切り出して、リンクとして表示
                                    echo anchor('comment/detail/'.$thrId.'/'.$commentNum, $linkStr); 
                                    # 「>>数値」をコメント内容から削除
                                    $commentContent = str_replace($linkStr, '', $commentContent);
                                }
                                echo $commentContent;
                            } else {
                                echo $commentContent;
                            }
                            ?>    
                        </div>   
                    <?php endforeach; ?>
                    <hr>

                    <?php 
                    if ($fbUserId != 0): # ログイン中
                    ?>
                        <?php  
                        $this->load->helper('form');
                        echo form_open('thread/index');
                        ?>
                            <div class="form-group">
                                <?php
                                # 返信内容
                                # 書き込んだスレと同じではない時
                                $sessionCommentContentTmp = '';
                                if ($thrId == $sessionThreadId) {
                                    $sessionCommentContentTmp = $sessionCommentContent;
                                }
                                echo form_textarea(array(
                                    'class' => 'form-control',
                                    'id' => 'commentContent'.$thrId,
                                    'label' => false,
                                    'name' => 'commentContent',
                                    'placeholder' => '半角英数字のみしか入力できません',
                                    'value' => $sessionCommentContentTmp
                                ));
                                ?>
                            </div>
                            <?php
                            echo form_hidden(array(
                                'thrId' => $thrId,
                            ));
                            ?>
                            <?php
                            echo form_submit(array(
                                'class' => 'btn btn-default btn-sm pull-right',
                                'value' => '返信'
                            ));
                            ?>
                        <?php
                        echo form_close();
                        ?>  
                        <div style="clear: both;"></div>
                        <hr> 
                    <?php endif; ?> 
                </div>                
                <br><br>
            </div>
        </div>
    </div>
</body>
</html>