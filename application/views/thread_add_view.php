<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>エウレカ課題</title>
    <!-- 自作CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/eureka.css" />
    <!-- Angular.js -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.min.js"></script>
    <script src="<?php echo base_url(); ?>js/app.js"></script>
    <script src="<?php echo base_url(); ?>js/thread.js"></script>
</head>
<body>
    <?php echo $this->load->view('header', $fb); ?>

    <div ng-app="myApp" class="container" style="margin-top: 70px; margin-bottom: 70px;">

        <div ng-controller="AppController">

            <div ng-controller="ThreadController">

                <?php
                $this->load->helper('form');
                echo form_open('thread/add');
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            タイトル<br>
                            <?php
                            echo form_input(array(
                                'label' => false,
                                'div' => false,
                                'class' => 'form-control',
                                'name' => 'title',
                                'ng-model' => 'title',
                                'placeholder' => '半角英数字のみしか入力できません'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            内容<br>
                            <?php
                            echo form_textarea(array(
                                'label' => false,
                                'div' => false,
                                'class' => 'form-control',
                                'name' => 'content',
                                'ng-model' => 'content',
                                'placeholder' => '半角英数字のみしか入力できません'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <button ng-click="addThread()" class="btn btn-primary">新規登録</button>
                <?php
                // echo form_submit(array(
                //     'class' => 'btn btn-primary',
                //     'value' => '新規登録'
                // ));
                echo form_close();
                ?>
                <br><br>
            </div>
        </div>
    </div>
</body>
</html>