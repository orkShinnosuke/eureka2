<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>エウレカ課題</title>
    <!-- Angular.js -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.min.js"></script>
    <script src="../../js/app.js"></script>
    <script src="../../js/user.js"></script>
</head>
<body>
    <?php echo $this->load->view('header', $fb); ?>

    <div ng-app="myApp" class="container" style="margin-top: 70px;">

        <div ng-controller="AppController">

            <div ng-controller="UserController">
                <!-- 一覧表 -->
                <table class="table table-striped">
                    <tr>
                        <th>facebookのID</th>
                        <!-- <td><?php echo $facebookId; ?></td> -->
                        <td>{{user.0.facebook_id}}</td>
                    </tr>
                    <tr>
                        <th>名前</th>
                        <!-- <td><?php echo $lastname.' '.$firstname; ?></td> -->
                        <td>{{user.0.lastname}} {{user.0.firstname}}</td>
                    </tr>
                    <tr>
                        <th>性別</th>
                        <!-- <td><?php echo $gender; ?></td> -->
                        <td>{{user.0.gender}}</td>
                    </tr>
                    <tr>
                        <th>プロフィール画像</th>
                        <!-- <td><?php echo $profileImg; ?></td> -->
                        <td>{{user.0.profile_img}}</td>
                    </tr>
                    <tr>
                        <th>初ログイン日時</th>
                        <!-- <td><?php echo $created; ?></td> -->
                        <td>{{user.0.created}}</td>
                    </tr>
                    <tr>
                        <th>最終ログイン日時</th>
                        <!-- <td><?php echo $modified; ?></td> -->
                        <td>{{user.0.modified}}</td>
                    </tr>
                </table>

            </div>
        </div>

    </div>
</body>
</html>