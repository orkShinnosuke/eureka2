<!-- Bootstrap -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.min.css" />
<!-- jQuery -->
<script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
<!-- UI Bootstrap -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>
<script language="javascript">
    angular.module("MyApp", ["ui.bootstrap"]);
</script>

<!-- ナビゲーションバー -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <?php 
            # urlヘルパーの読み込み
            $this->load->helper('url');
            echo anchor('thread/index', 'ホーム', array('class' => 'navbar-brand'));
            ?>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <?php 
            $userId = $fb->getUser();            
            if ($userId == 0): ?>
                <li>
                    <?php 
                    echo anchor('user/login', 'Facebookでログイン'); 
                    ?>
                </li>
            <?php else: ?>
                <?php  
                $me = $fb->api('/me','GET',array('locale'=>'ja_JP')); # ユーザ情報を日本語で取得
                ?>
                <li>
                    <?php echo anchor('user/logout', 'ログアウト', array('class' => 'pull-right')); ?>
                    <div class="pull-right">
                        <div style="padding-top: 15px; padding-bottom: 15px; padding-right: 10px; color: white;">
                            <?php 
                            echo anchor('user/info', $me['last_name'].' '.$me['first_name']);
                            ?>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div style="padding-top: 10px; padding-bottom: 10px; padding-right: 7px;">
                            <a href="../user/info">
                                <img src="https://graph.facebook.com/<?php echo $me['id']; ?>/picture" style="width: 30px; cursor: pointer;">
                            </a>
                        </div>
                    </div>
                </li>                
            <?php endif; ?>
        </ul>
    </div>
</nav>
