<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>エウレカ課題</title>
    <!-- 自作CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/eureka.css" />
    <!-- Angular.js -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.min.js"></script>
    <script src="<?php echo base_url(); ?>js/app.js"></script>
    <script src="<?php echo base_url(); ?>js/comment.js"></script>
</head>
<body>
    <?php echo $this->load->view('header', $fb); ?>

    <div ng-app="myApp" class="container" style="margin-top: 70px; margin-bottom: 70px;">
        <div ng-controller="AppController">
            <div ng-controller="CommentController">

                <legend>返信内容確認</legend>
                <h5><b>返信内容</b></h5>
                <?php  
                $commentContent = $commentArray['commentContent'];
                $thrId = $commentArray['thrId'];
                echo $commentContent;
                ?>
                <br><br>
                <?php  
                $this->load->helper('form');
                echo form_open('comment/confirm');
                echo form_hidden(array(
                    'commentContent' => $commentContent,
                    'ng-model' => 'commentContent'
                ));
                echo form_hidden(array(
                    'thrId' => $thrId,
                    'ng-model' => 'thrId'
                ));
                // echo form_submit(array(
                //     'class' => 'btn btn-default',
                //     'name' => 'responseDone',
                //     'value' => '返信完了'
                // ));
                ?>
                <button ng-click="addComment()" class="btn btn-default">返信完了</button>
                <?php
                echo form_close();
                ?>
            </div>
        </div>
    </div>
</body>
</html>