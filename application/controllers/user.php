<?php 
class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');     
    }   

	public function createFacebook()
    {
        # ライブラリ読み込み
        $config = array(
            'appId'  => APP_ID,
            'secret' => APP_SECRET
        );
        $this->load->library('facebook', $config);

        # インスタンス生成
        $fb = new Facebook(array(
                'appId' => APP_ID,
                'secret' => APP_SECRET
            )
        );
        return $fb;
	}

    public function login()
    {
        # urlヘルパーの読み込み
        $this->load->helper('url');

        # ログインユーザ情報取得
        $fb = $this->createFacebook();
        $userId = $fb->getUser();

        if ($userId != 0) { # 認証後
            $me = $fb->api('/me','GET',array('locale'=>'ja_JP')); # ユーザ情報を日本語で取得
            
            # fbデータをセッションに保存
            $this->load->library('session');
            $this->session->set_userdata('myFbData', $me);

            # 初ログインの時か否か判断
            $this->load->model('user_dao');
            $user = $this->user_dao->getUserByFacebookId($userId, 'id');

            # 初ログインの時
            if (is_array($user) && count($user) == 0) {
                # ユーザー情報を保存
                $this->user_dao->insertUser($me);

            # 以前にログインしたことがある時
            } else {
                # 更新日時のみ更新
                $data = array('modified' => date('Y-m-d H:i:s'));
                $this->db->update('users', $data, array('facebook_id' => $userId));
            }

            # ホーム画面へ
            redirect('thread/index', 'refresh');
        # 認証前
        } else {
            $url = $fb->getLoginUrl(array(
                'scope' => 'email',
                'canvas' => 1,
                'fbconnect' => 0,
            ));

            # 認証画面へ
            redirect($url, 'refresh');
        }
    }

    public function logout()
    {
        $fb = $this->createFacebook();
        $fb->destroySession(); # セッション削除
        # fbデータをセッションに保存
        $this->load->library('session');
        $this->session->unset_userdata('myFbData');

        # ホーム画面へ
        $this->load->helper('url');
        redirect('thread/index', 'refresh');
    }

    public function info()
    {
        # facebookのIDを取得
        $fb = $this->createFacebook();
        $data['fb'] = $fb;
        $userId = $fb->getUser();

        # レンダリング
        $this->load->view('user_info_view', $data);
    }

    public function view()
    {
        # facebookのIDを取得
        $fb = $this->createFacebook();
        $userId = $fb->getUser();

        # usersテーブルからデータ取得
        $this->load->model('user_dao');
        $user = $this->user_dao->getUserByFacebookId($userId, 'id, facebook_id, lastname, firstname, gender, profile_img, created, modified');
        echo json_encode($user);
    }
}
