<?php 
class Comment extends CI_Controller
{
	public function createFacebook()
    {
        # ライブラリ読み込み
        $config = array(
            'appId'  => APP_ID,
            'secret' => APP_SECRET
        );
        $this->load->library('facebook', $config);

        # インスタンス生成
        $fb = new Facebook(array(
                'appId'  => APP_ID,
                'secret' => APP_SECRET
            )
        );
        return $fb;
	}

    public function confirm()
    {
        # FBデータ取得
        $fb = $this->createFacebook();
        $data['fb'] = $fb;

        # コメント取得
        $this->load->library('session');
        $commentArray = $this->session->userdata('commentArray');
        $data['commentArray'] = $commentArray;

        # レンダリング
        $this->load->view('comment_confirm_view', $data);

        if ($this->input->post()) {
            # コメント内容をセッションから削除
            $this->load->library('session');
            $this->session->unset_userdata('commentArray');

            # DAOに接続
            $this->load->model('comment_dao');
             # コメント情報を保存
            $userId = $fb->getUser();
            $this->comment_dao->insertComment($this->input->post(), $userId);

            # 返信つけたら、スレッドのmodifiedを更新
            $thrId = $this->input->post('thrId');
            $this->db->update( 'threads', array('modified' => date('Y-m-d H:i:s')), array('id' => $thrId) );

            # 一覧ページへ
            $this->load->helper('url');
            redirect('thread/index', 'refresh');
        }
    }

    public function detail($thrId, $comNum)
    {
        $data['thrId'] = $thrId;
        $data['comNum'] = $comNum;

        # FBデータ取得
        $fb = $this->createFacebook();
        $data['fb'] = $fb;

        # DAOに接続
        $this->load->model('thread_dao');

        # スレッド情報を取得
        $thread = $this->thread_dao->getById($thrId);
        $data['thread'] = $thread;

        # threadsテーブルからデータ取得
        $fields = 'id, thread_id, user_id, content, picture, fb_picture, created';
        $comments = $this->thread_dao->getThreadComments($thrId, $fields);
        # -2の理由：配列の添字は0から始まるので-1、さらにスレ内容自体をコメント内容と考えるので-1
        $comment = $comments[$comNum-2];
        $data['comment'] = $comment;

        # レンダリング
        $this->load->view('comment_detail_view', $data);
    }
}
