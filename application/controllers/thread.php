<?php 
class Thread extends CI_Controller
{
    public function createFacebook()
    {
        # ライブラリ読み込み
        $config = array(
            'appId'  => APP_ID,
            'secret' => APP_SECRET
        );
        $this->load->library('facebook', $config);

        # インスタンス生成
        $fb = new Facebook(array(
                'appId'  => APP_ID,
                'secret' => APP_SECRET
            )
        );
        return $fb;
	}

    public function index()
    {
        # FBデータ取得
        $fb = $this->createFacebook();
        $data['fb'] = $fb;

        # サービスに接続
        $this->load->model('thread_service');

        # 上位10件のスレ
        $threadsTop10 = $this->thread_service->getThreadsTop10();
        $data['threadsTop10'] = $threadsTop10;

        # 上位10件のスレへのコメント
        $comments = $this->thread_service->getComments($threadsTop10);
        $data['comments'] = $comments;

        # FacebookのログインユーザーID
        $fbUserId = 0;
        # ログイン中か否かを判断。ログイン中なら$myFbDataの中身あり。キーが存在しない場合はnullを返す
        $this->load->library('session');
        $myFbData = $this->session->userdata('myFbData');
        if ($myFbData != null) {
            $fbUserId = $fb->getUser();
        }
        $data['fbUserId'] = $fbUserId;

        # もしセッションにスレIDとFBフォトURLがあれば、ビューに渡す
        $data['sessionThreadId'] = $this->session->userdata('threadId');
        $data['sessionFbPhotoUrl'] = $this->session->userdata('fbPhotoUrl');
        $data['sessionCommentContent'] = $this->session->userdata('commentContent');

        # レンダリング
        $this->load->view('thread_index_view', $data);

        if ($this->input->post()) {
            # バリデーションライブラリ読み込み
            $this->load->library('form_validation');
            # レスが 空文字 or 英数字 ならfalse 
            $this->form_validation->set_rules('commentContent', '内容', 'required|alpha_numeric');   

            # バリデーション
            if ($this->form_validation->run()) {
                $this->session->set_userdata('commentArray', $this->input->post());
                # 書き込み確認ページへ
                redirect('comment/confirm', 'refresh');
            } else {
                # 元のページへ戻る
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            }
        }
    }

    public function getThreadsTop100()
    {
        # サービスに接続
        $this->load->model('thread_service');

        $threadsTop100 = $this->thread_service->getThreadsTop100();
        echo json_encode($threadsTop100);
    }

    public function detail($thrId)
    {
        # DAOに接続
        $this->load->model('thread_dao');

        # スレッド情報を取得
        $thread = $this->thread_dao->getById($thrId);
        $data['thread'] = $thread;

        # threadsテーブルからデータ取得
        $fields = 'id, thread_id, user_id, content, picture, fb_picture, created';
        $comments = $this->thread_dao->getThreadComments($thrId, $fields);
        $data['comments'] = $comments;

        # FBデータ取得
        $fb = $this->createFacebook();
        $data['fb'] = $fb;

        # FacebookのログインユーザーID
        $fbUserId = 0;
        # ログイン中か否かを判断。ログイン中なら$myFbDataの中身あり。キーが存在しない場合はnullを返す
        $this->load->library('session');
        $myFbData = $this->session->userdata('myFbData');
        if ($myFbData != null) {
            $fbUserId = $fb->getUser();
        }
        $data['fbUserId'] = $fbUserId;

        # もしセッションにスレIDとFBフォトURLがあれば、ビューに渡す
        $data['sessionThreadId'] = $this->session->userdata('threadId');
        $data['sessionFbPhotoUrl'] = $this->session->userdata('fbPhotoUrl');
        $data['sessionCommentContent'] = $this->session->userdata('commentContent');

        $this->load->view('thread_detail_view', $data);
    }

    public function add()
    {
        # FBデータ取得
        $fb = $this->createFacebook();
        $data['fb'] = $fb;

        # レンダリング
        $this->load->view('thread_add_view', $data);

        if ($this->input->post()) {
            $userId = $fb->getUser();

            # DAOに接続
            $this->load->model('thread_dao');
            $this->thread_dao->insertThread($this->input->post(), $userId);

            # 一覧確認ページへ
            redirect('thread/index', 'refresh');
        }
    }
}
