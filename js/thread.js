(function() {
    var ajs = angular.module('myApp.controllers', []);
    ajs.controller('AppController', function($scope, $rootScope, $http, $location) {
        $rootScope.appUrl = 'http://' + $location.host() + '/eureka2/index.php';
        // $rootScope.appUrl = "http://orkmacplay.local/eureka2/index.php";
    });
 
    ajs.controller('ThreadController', function($scope, $rootScope, $http) {

        // 一覧表示(thread_index)
        var load = function() {
            console.log('load中です。');
            // 上位100件のスレタイトル
            $http.get($rootScope.appUrl + '/thread/getThreadsTop100').success(function(data) {
                $scope.threadsTop100 = data;
            }).error(function(data) {
                $scope.errors = data;
            });
            // // 上位10件のスレ
            // $http.get($rootScope.appUrl + '/thread/getThreadsTop10').success(function(data) {
            //     $scope.threadsTop10 = data;
            // }).error(function(data) {
            //     $scope.errors = data;
            // });
            // // 上位10件のスレへのコメント
            // $http.get($rootScope.appUrl + '/thread/getComments').success(function(data) {
            //     $scope.comments = data;
            // }).error(function(data) {
            //     $scope.errors = data;
            // });
        };
        load();

        // 登録(thread_add)
        $scope.addThread = function() {
            $http.post($rootScope.appUrl + '/thread/add', {
                'title': $scope.title,
                'content': $scope.content,
            }).success(function(){
                // alert('登録しました。');
            }).error(function(data, status) {
                // $scope.errors = data;
                // alert('失敗しました。');
            });
        };
    });

}());