(function() {
    var ajs = angular.module('myApp.controllers', []);
    ajs.controller('AppController', function($scope, $rootScope, $http, $location) {
        $rootScope.appUrl = 'http://' + $location.host() + '/eureka2/index.php';
        // $rootScope.appUrl = "http://orkmacplay.local/eureka2/index.php";
    });
 
    ajs.controller('CommentController', function($scope, $rootScope, $http) {

        // 登録(comment_confirm)
        $scope.addComment = function() {
            $http.post($rootScope.appUrl + '/comment/confirm', {
                'commentContent': $scope.commentContent,
                'thrId': $scope.thrId,
            }).success(function(){
                console.log('成功しました。');
            }).error(function(data, status) {
                // $scope.errors = data;
                console.log('失敗しました。');
            });
        };
    });

}());